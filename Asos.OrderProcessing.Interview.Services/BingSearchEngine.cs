using System.IO;
using System.Net;

namespace Asos.OrderProcessing.Interview.Services
{
	public class BingSearchEngine : SearchEngine, ISearchEngine
	{
		private readonly int _timeout;

		public BingSearchEngine()
		{
			_timeout = 5000;
			BaseUrl = "http://www.bing.com/search?q=";
		}

		public string Search(string query)
		{
			var request = HttpWebRequest.Create(BaseUrl + query);
			request.Timeout = _timeout;
			var response = request.GetResponse();
			var stream = response.GetResponseStream();
			if (stream == null)
				return string.Empty;
			using(var sr = new StreamReader(stream))
			{
				return sr.ReadToEnd();
			}
		}
	}
}