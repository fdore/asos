using Asos.OrderProcessing.Interview.Contracts;
using NServiceBus;

namespace Asos.OrderProcessing.Interview.Services.Handlers
{
	public class SearchRequestHandler : IHandleMessages<SearchEngineRequest>
	{
		private readonly ISearchEngine _searchEngine;
		public IBus Bus { get; set; }

		public SearchRequestHandler() : this(new BingSearchEngine())
		{
			
		}

		public SearchRequestHandler(ISearchEngine searchEngine)
		{
			_searchEngine = searchEngine;
		}

		public SearchRequestHandler(IBus bus, ISearchEngine searchEngine)
		{
			Bus = bus;
			_searchEngine = searchEngine;
		}

		public void Handle(SearchEngineRequest request)
		{
			var result = _searchEngine.Search(request.Query);
			
			Bus.Reply(new SearchEngineResponse
			          	{
			          		Result = result,
							Query = request.Query
			          	});
		}
	}
}