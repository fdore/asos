namespace Asos.OrderProcessing.Interview.Services
{
	public interface ISearchEngine
	{
		string Search(string query);
	}
}