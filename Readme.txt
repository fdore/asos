For the purpose of this exercise, I have assumed we were in the context of an order processing application.

Such an application would have to process a large number of transactions (in our case, 100 requests), and interact with multiple 3rd parties (simulated by external calls to bing.com).

Therefore, I have opted for a distributed architecture, using publish/subscribe communication, which offers loose coupling, resilience, and scalability.

I have chosen NServiceBus to provide service bus capabilities. The principles would be the same with other ServiceBus frameworks.

The application is made of a Client (Asos.OrderProcessing.Interview.Client.exe), which sends 100 search requests, and a Server (Asos.OrderProcessing.Interview.Services.Host.exe), which processes the requests in an asynchronous fashion.

Requests to bing.com are configured to time out after 10 seconds, after which an exception would be thrown.
The exception would cause NServiceBus to retry handling the message a number of times, until the message ends in an error queue, that could be processed later if the outage was to last for a long time.

I have used xUnit for the unit tests, which enforces AAA principles, and offers BDD style unit tests.

I'm using dependency injection so that we can easily test by injecting and mocking dependencies.

Limitations:
This application is using the trial version of NServiceBus, which restricts the number of worker threads to 1.

Instructions:
To debug the application, configure the solution to start both Asos.OrderProcessing.Interview.Client and Asos.OrderProcessing.Interview.Services.Host.
Add NServiceBus.Integration to the command line arguments for both the client and the server host projects to make sure the queues are created at startup.


Otherwise, run the following command lines:
Asos.OrderProcessing.Interview.Services.Host.exe NServiceBus.Integration
Asos.OrderProcessing.Interview.Client.exe NServiceBus.Integration

Note that the Server has to be started first so that the queues exist when the client tries to send requests.

Prerequesites:
You will need to have MSMQ installed on your machine.



