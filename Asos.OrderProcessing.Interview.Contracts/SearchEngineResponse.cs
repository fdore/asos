namespace Asos.OrderProcessing.Interview.Contracts
{
	public class SearchEngineResponse : IMessage
	{
		public string Query { get; set; }
		public string Result { get; set; }
	}
}