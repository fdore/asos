namespace Asos.OrderProcessing.Interview.Contracts
{
	public class SearchEngineRequest : IMessage
	{
		public string Query { get; set; }
	}
}