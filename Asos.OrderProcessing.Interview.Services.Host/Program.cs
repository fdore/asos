﻿using System;
using NServiceBus;

namespace Asos.OrderProcessing.Interview.Services.Host
{
	class Program
	{
		static void Main(string[] args)
		{
			Configure.With()
			   .Log4Net()
			   .DefaultBuilder()
			   .XmlSerializer()
			   .MsmqTransport()
				   .IsTransactional(true)
				   .PurgeOnStartup(false)
				   .InMemorySubscriptionStorage()
				   .DefiningMessagesAs(t => typeof(Contracts.IMessage).IsAssignableFrom(t))
			   .UnicastBus()
				   .LoadMessageHandlers()
			   .CreateBus()
			   .Start(() => Configure.Instance.ForInstallationOn<NServiceBus.Installation.Environments.Windows>().Install());

			Console.Read();
		}
	}
}
