﻿using System;
using System.Threading;
using Asos.OrderProcessing.Interview.Contracts;
using NServiceBus;

namespace Asos.OrderProcessing.Interview.Client
{
	public class Program
	{
		private static IBus _bus;

		private static void BusBootstrap()
		{
			_bus = Configure.With()
				.Log4Net()
				.DefaultBuilder()
				.XmlSerializer()
				.MsmqTransport()
					.IsTransactional(true)
					.PurgeOnStartup(false)
					.DefiningMessagesAs(t => typeof(Contracts.IMessage).IsAssignableFrom(t))
				.UnicastBus()
					.LoadMessageHandlers()
				.CreateBus()
				.Start(() => Configure.Instance.ForInstallationOn<NServiceBus.Installation.Environments.Windows>().Install());
		}

		static void Main()
		{
			try
			{
				BusBootstrap();
				Thread.Sleep(5000);
				for (int i = 0; i < 100; i++ )
				{
					Console.WriteLine("Sending request ...");
					_bus.Send(new SearchEngineRequest
					{
						Query = "asos " + i
					});
				}
					
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
			Console.ReadKey();
		}
	}
}
