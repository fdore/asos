﻿using System;
using Asos.OrderProcessing.Interview.Contracts;
using NServiceBus;

namespace Asos.OrderProcessing.Interview.Client
{
	public class SearchResultMessageHandler : IHandleMessages<SearchEngineResponse>
	{
		public void Handle(SearchEngineResponse message)
		{
			Console.WriteLine(string.Format("Received result for {0}. Response: {1}", message.Query, message.Result));
		}
	}
}
