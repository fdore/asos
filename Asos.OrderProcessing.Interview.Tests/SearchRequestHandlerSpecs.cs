﻿using Asos.OrderProcessing.Interview.Contracts;
using Asos.OrderProcessing.Interview.Services;
using Asos.OrderProcessing.Interview.Services.Handlers;
using FakeItEasy;
using NServiceBus;
using SubSpec;

namespace Asos.OrderProcessing.Interview.Tests
{
	public class SearchRequestHandlerSpecs
	{
		[Specification]
		public void RequestHandlerShouldReturnCorrectResponseMessage()
		{
			var handler = default(SearchRequestHandler);
			var request = default(SearchEngineRequest);
			var bus = default(IBus);
			var called = false;
			@"Given we have a search request handler,
			  And we search for asos
			  And the search engine returns 'content'"
				.Context(() =>
				         	{
				         		var searchEngine = A.Fake<ISearchEngine>();
				         		A.CallTo(() => searchEngine.Search(A<string>.Ignored))
				         			.Returns("content");

				         		bus = A.Fake<IBus>();
								handler = new SearchRequestHandler(bus, searchEngine);
				         		request = new SearchEngineRequest
				         		          	{
				         		          		Query = "asos"
				         		          	};
				         	});

			"When handling the request".Do(() => handler.Handle(request));

			"Then 'content' should be sent back"
				.Observation(
					() => A.CallTo(() => bus.Reply(A<SearchEngineResponse>.That.Matches(response => response.Result.Equals("content")))).MustHaveHappened());

			"Then the response should contain the query 'asos'"
				.Observation(
					() => A.CallTo(() => bus.Reply(A<SearchEngineResponse>.That.Matches(response => response.Query.Equals("asos")))).MustHaveHappened());
			
		}
	}
}
